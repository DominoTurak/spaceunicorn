﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityActivator : MonoBehaviour {

	public void Start(){
		this.GetComponent<CircleCollider2D> ().radius = this.transform.parent.GetComponent<PlanetGravity> ().soiRadius * 1.5f;

	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Ship") {
			this.transform.parent.GetComponent<PlanetGravity> ().SubscribeGravityBody (other.gameObject);
		}
	}

	public void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.tag == "Ship") {
			this.transform.parent.GetComponent<PlanetGravity> ().UnsubscribeGravityBody (other.gameObject);
		}
	}


}

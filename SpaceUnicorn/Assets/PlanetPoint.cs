﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetPoint : MonoBehaviour {

	public Planet currentPlanet;

	public bool closeToShip = false;

	public PlanetSetting planetSettings;

	void OnTriggerEnter2D(Collider2D other){


		if (other.gameObject.tag == "Ship" && currentPlanet == null) {
			//this.currentPlanet = PlanetsController.Instance.AddPlanet (this);
			Planet newPlanet = PlanetsController.Instance.RequestPlanet();
			this.currentPlanet = newPlanet;
			if (newPlanet != null) {
				SetPlanetSetting ();
				newPlanet.transform.position = this.transform.position;
				closeToShip = true;
			}

		}


	}

	void OnTriggerExit2D(Collider2D other){

		if (other.gameObject.tag == "Ship") {
			//PlanetsController.Instance.RemovePlanet(this);
			PlanetsController.Instance.FreePlanet(this.currentPlanet);
			this.currentPlanet = null;
			closeToShip = false;
		}
	}

	public void SetPlanetSetting(){
		if (currentPlanet != null) {
			PlanetGravity gravity = currentPlanet.GetComponent<PlanetGravity> ();
			currentPlanet.transform.localScale = new Vector3 (planetSettings.planetScale, planetSettings.planetScale, 1);

			gravity.mass = planetSettings.mass;
			gravity.proximityModifier = (int)planetSettings.proximityModifier;
			gravity.soiRadius = (int)planetSettings.soiRadius;

			currentPlanet.GetComponent<SpriteRenderer> ().sprite = planetSettings.planetSprite;
		}
	}


	void OnDrawGizmos(){
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(transform.position, 1);
	}



}

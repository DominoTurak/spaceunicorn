﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetsController : Singleton<PlanetsController> {

	private List<Transform> InactivePlanetPoints;
	private List<Transform> ActivePlanetPoints;

	private List<Planet> InactivePlanets;
	private List<Planet> ActivePlanets;

	public Transform startPoint;

	public int InitialPlanetPoints;
	public int InitialPlanets;

	public Transform PlanetPointPrefab;
	public Planet PlanetPrefab;


	public int GenerationBlockDistance;
	public int GenerationBlockCount;

	public float minSoi;
	public float maxSoi;

	public float minSize;
	public float maxSize;
	public float sizeVariability;
	public float minMass;
	public float maxMass;
	public float maxProxModifier;
	public float minProxModifier;

	public List<Sprite> PlanetSprites;



	private Transform lastGeneratedPoint;
	private Transform lastGeneratedAditionalPoint;

	private Camera mainCam;

	private Transform initialPlanetPoint;


	public Vector2 minPlanetsDistance;
	public Vector2 maxPlanetsDistance;

	private float minPDistance;
	private float maxPDistance;


	public float fuelDistanceSpawnFrequency;
	public float fuelSpawnDistance;
	private float lastFuelSpawn;


	private PlanetSetting default_setting;
	// Use this for initialization
	void Start () {



		minPDistance = Mathf.Sqrt (Mathf.Pow (minPlanetsDistance.x, 2) + Mathf.Pow (minPlanetsDistance.y, 2));
		maxPDistance = Mathf.Sqrt (Mathf.Pow (maxPlanetsDistance.x, 2) + Mathf.Pow (maxPlanetsDistance.y, 2));

		mainCam = Camera.main;

		InactivePlanetPoints = new List<Transform> ();
		ActivePlanetPoints = new List<Transform> ();

		InactivePlanets = new List<Planet> ();
		ActivePlanets = new List<Planet> ();


		Transform planetPointHolder = this.transform.GetChild (0);
		Transform planetHolder = this.transform.GetChild (1);

		for (int i = 0; i < InitialPlanetPoints; i++) {

			Transform newPlanetPoint = Transform.Instantiate (PlanetPointPrefab, new Vector3(-100,0,0), Quaternion.Euler (Vector3.zero), planetPointHolder);
			newPlanetPoint.name = "PlanetPoint_" + i.ToString ();
			InactivePlanetPoints.Add (newPlanetPoint);
		}

		for (int j = 0; j < InitialPlanets; j++) {
			GameObject newPlanet = Transform.Instantiate (PlanetPrefab.gameObject, new Vector3(-100,0,0), Quaternion.Euler (Vector3.zero), planetHolder) as GameObject;
			newPlanet.name = "Planet_" + j.ToString ();
			InactivePlanets.Add (newPlanet.GetComponent<Planet>());
		}



		default_setting  = new PlanetSetting ();

		default_setting.planetScale = (minSize + maxSize) / 2;
		default_setting.mass = (minSize + maxSize) / 2;
		default_setting.soiRadius = (int)((minSoi + maxSoi) / 2);
		default_setting.proximityModifier = (int)(minProxModifier + maxProxModifier) / 2;

		Sprite planetSprite = PlanetSprites [(int)Random.Range (0, PlanetSprites.Count - 1)];

		default_setting.planetSprite = planetSprite == null ? PlanetSprites [0] : planetSprite;


		lastGeneratedPoint = startPoint;

		StartCoroutine (GeneratePlanetPoints ());

	}

	public void Update(){
		if(Input.GetKeyDown(KeyCode.S)){
			ResetAll ();
		}
	}

	public IEnumerator GeneratePlanetPoints(){

		while (true) {

			if (ShouldGenerateNewBlock () && lastGeneratedPoint != null) {
				GenerateNewBlock ();
			} else {
				if (GameManager.Instance.Ship.transform.position.y > lastFuelSpawn + fuelDistanceSpawnFrequency) {
					float fuelSpawnY = lastFuelSpawn + fuelSpawnDistance + fuelDistanceSpawnFrequency;
					FuelController.Instance.ActivateFuel (new Vector3 (Random.Range (-GameManager.Instance.MapWidth + 10, GameManager.Instance.MapWidth - 10), fuelSpawnY, 0));
					lastFuelSpawn = fuelSpawnY;
				}
			}
			yield return null;
		}

	}

	private bool ShouldGenerateNewBlock(){
		if (Camera.main.transform.position.y + GenerationBlockDistance > lastGeneratedPoint.position.y + minPlanetsDistance.y) {
			return true;
		} else {
			return false;
		}

	}

	private void GenerateNewBlock(){

		float height = Random.Range (minPlanetsDistance.y, maxPlanetsDistance.y);
		float width = Random.Range (minPlanetsDistance.x, maxPlanetsDistance.x);
		if ( lastGeneratedPoint.position.x + width >= GameManager.Instance.MapWidth / 2 || lastGeneratedPoint.position.x + width <= -GameManager.Instance.MapWidth / 2 ) {
			width = -1 * width;
		}
		Vector3 newPos = new Vector3 (width + lastGeneratedPoint.position.x, height + lastGeneratedPoint.position.y, 0);

		Transform planetPointToSet = AddNewPlanetPoint (newPos);
		planetPointToSet.GetComponent<PlanetPoint> ().planetSettings = GetPlanetSettings (newPos);

		if (Mathf.Abs (newPos.x) > GameManager.Instance.MapWidth / 2 / 10) {
			float AditionalHeight = height - Random.Range (0, maxPlanetsDistance.y / 1.3f);
			float AditionalWidth = newPos.x > 0 ? -Random.Range (GameManager.Instance.MapWidth / 2 / 5, GameManager.Instance.MapWidth / 2) : Random.Range (GameManager.Instance.MapWidth / 2 / 5, GameManager.Instance.MapWidth / 2);
			Vector3 addPos = new Vector3 (AditionalWidth, AditionalHeight + lastGeneratedPoint.position.y, 0);
			if (planetDistancesCheck (addPos)) {
				Transform additionalPlanetPoint = AddNewPlanetPoint (addPos);
				additionalPlanetPoint.GetComponent<PlanetPoint> ().planetSettings = GetPlanetSettings (addPos);
				lastGeneratedAditionalPoint = additionalPlanetPoint;
			} else {
				lastGeneratedAditionalPoint = null;
			}
		} else {
			lastGeneratedAditionalPoint = null;
		}

		lastGeneratedPoint = planetPointToSet;

	}

	private bool planetDistancesCheck(Vector3 newPosition){

		if (Vector3.Distance (lastGeneratedPoint.position, newPosition) > minPDistance) {
			if (lastGeneratedAditionalPoint != null) {
				if (Vector3.Distance (lastGeneratedAditionalPoint.position, newPosition) > minPDistance) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}

	}

	private PlanetSetting GetPlanetSettings(Vector3 position){
		float distanceToLastPoint = Vector3.Distance (position, lastGeneratedPoint.position);



		float distance = (minPDistance + maxPDistance) / 2;
		if (lastGeneratedAditionalPoint != null) {
			float distanceToLastAditionalPoint = Vector3.Distance (position, lastGeneratedAditionalPoint.position);
			distance = distanceToLastPoint <= distanceToLastAditionalPoint ? distanceToLastPoint : distanceToLastAditionalPoint;
			PlanetSetting planetToCompare = distanceToLastPoint <= distanceToLastAditionalPoint ? lastGeneratedPoint.GetComponent<PlanetPoint> ().planetSettings : lastGeneratedAditionalPoint.GetComponent<PlanetPoint> ().planetSettings;
		} else {
			distance = distanceToLastPoint;

			PlanetPoint settingToCompare = lastGeneratedPoint.GetComponent<PlanetPoint> ();
			if (settingToCompare != null) {
				PlanetSetting planetToCompare = settingToCompare.planetSettings;
			} else {
				PlanetSetting planetToCompare = default_setting;
			}

		}


		float sizeBase = (distance - minPDistance) / (maxPDistance - minPDistance);
		float sizeBaseMin = minSize + (maxSize - minSize)*sizeBase - sizeVariability < minSize ? minSize : minSize + (maxSize - minSize)*sizeBase - sizeVariability;
		float sizeBaseMax = minSize + (maxSize - minSize)*sizeBase + sizeVariability > maxSize ? maxSize : minSize + (maxSize - minSize)*sizeBase + sizeVariability;
		float size = Random.Range (sizeBaseMin, sizeBaseMax);

		size = size > maxSize ? maxSize : size;
		size = size < minSize ? minSize : size;

		size = Random.Range (minSize, maxSize);

		float sizePercent = ((size - minSize) / (maxSize - minSize));

		float baseSoi = minSoi + sizePercent * (maxSoi - minSoi);
		baseSoi = Mathf.CeilToInt (baseSoi);
		int soi = Mathf.Clamp (Mathf.CeilToInt (baseSoi), (int)minSoi, (int)maxSoi);

		float mass = minMass + sizePercent * (maxMass - minMass);
		mass = Mathf.Clamp (Mathf.CeilToInt (mass), (int)minMass, (int)maxMass);

		float proxModifier = minProxModifier + sizePercent * (maxProxModifier - minProxModifier);
		proxModifier = proxModifier < maxProxModifier ? maxProxModifier : proxModifier;
		proxModifier = proxModifier > minProxModifier ? minProxModifier : proxModifier;





		PlanetSetting setting = new PlanetSetting ();
		setting.planetScale = size;
		setting.soiRadius = soi;
		setting.mass = mass;
		setting.proximityModifier = (int)proxModifier;

		Sprite planetSprite = PlanetSprites [(int)Random.Range (0, PlanetSprites.Count - 1)];

		setting.planetSprite = planetSprite == null ? PlanetSprites [0] : planetSprite;


		return setting;
	}

	private Transform AddNewPlanetPoint(Vector3 position){
		Transform planetPointToSet;

		if (InactivePlanetPoints.Count > 0) {
			planetPointToSet = InactivePlanetPoints [0].transform;
			ActivePlanetPoints.Add (planetPointToSet);
			InactivePlanetPoints.Remove (planetPointToSet);

		} else {
			planetPointToSet = ActivePlanetPoints [0];
			ActivePlanetPoints.Remove (planetPointToSet);
			ActivePlanetPoints.Add (planetPointToSet);

		}

		if (planetPointToSet != null) {
			planetPointToSet.transform.position = position;
			return planetPointToSet;
		} else {
			Debug.LogError ("planetPoint that should be set is null");
			return null;
		}
	}


	public Planet AddPlanet(PlanetPoint point){
		if (InactivePlanets.Count > 0 && point.currentPlanet == null) {

			Planet planetToMove = InactivePlanets [0];

			planetToMove.transform.position = point.transform.position;

			planetToMove.gameObject.SetActive (true);

			ActivePlanets.Add (planetToMove);

			InactivePlanets.Remove (planetToMove);

			return planetToMove;
		} 
		return null;

	}

	public Planet RemovePlanet(PlanetPoint point){

		Planet planet = point.currentPlanet;

		if (planet != null) {
			ActivePlanets.Remove (planet);
			InactivePlanets.Add (planet);

		}
		return null;
	}

	public void FreePlanet(Planet planet){
		if (planet != null && ActivePlanets.Contains (planet)) {
			ActivePlanets.Remove (planet);
			InactivePlanets.Add (planet);
		}
	}

	public Planet RequestPlanet(){
		if (InactivePlanets.Count > 0) {
			Planet newPlanet = InactivePlanets [0];
			ActivePlanets.Add (newPlanet);
			InactivePlanets.Remove (newPlanet);
			return newPlanet;
		}
		return null;
	}

	public void ResetAll(){

		StopAllCoroutines ();


		for (int i = ActivePlanets.Count - 1; i >= 0 ; i--) {
			Planet planet = ActivePlanets [i];
			InactivePlanets.Add (planet);
			ActivePlanets.Remove (planet);
			Debug.Log ("Deactivating");
		}

		for (int j = ActivePlanetPoints.Count - 1; j >= 0 ; j--) {
			Transform point = ActivePlanetPoints [j];
			InactivePlanetPoints.Add (point);
			ActivePlanetPoints.Remove (point);

		}

		for (int l = 0; l < InactivePlanetPoints.Count; l++) {
			InactivePlanetPoints[l].position = new Vector3 (-200, -200, 0);

		}

		for (int k = 0; k < InactivePlanets.Count; k++) {
			InactivePlanets[k].transform.position = new Vector3 (-200, 0, 0);

		}

		lastGeneratedPoint = startPoint;
		lastGeneratedAditionalPoint = null;
		lastFuelSpawn = 0;

		StartCoroutine (GeneratePlanetPoints ());
	}

}

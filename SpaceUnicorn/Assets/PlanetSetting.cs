﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PlanetSetting {


	public float mass;
	public int soiRadius;
	public float proximityModifier;

	public float planetScale;
	public Sprite planetSprite;

}

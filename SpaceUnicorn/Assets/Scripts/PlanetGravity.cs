﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class PlanetGravity : MonoBehaviour { 


	public float mass;     			
	public int soiRadius;			
	public int proximityModifier = 195;	

	private Rigidbody2D ShipBody;

	public void Start(){

	}


	public void SubscribeGravityBody(GameObject body){
		this.ShipBody = body.GetComponent<Rigidbody2D> ();

	}

	public void UnsubscribeGravityBody(GameObject body){
		this.ShipBody = null;
	}

	public void OnDrawGizmos() { 
		
		Gizmos.DrawWireSphere (transform.position, soiRadius);
	}

	void FixedUpdate () { // Runs continuously during gameplay



		if (ShipBody != null) {

			Rigidbody2D gravRigidBody = ShipBody; 

			float orbitalDistance = Vector3.Distance (transform.position, gravRigidBody.transform.position); 

			if (orbitalDistance < soiRadius) { 


				Vector3 objectOffset = transform.position - gravRigidBody.transform.position;
				objectOffset.z = 0;

				Vector3 objectTrajectory = gravRigidBody.velocity; 

				float angle = Vector3.Angle (objectOffset, objectTrajectory); 

				float magsqr = objectOffset.sqrMagnitude; 

				if ( magsqr > 0.0001f ) { 


					Vector3 gravityVector = ( mass * objectOffset.normalized / magsqr ) * gravRigidBody.mass;
					gravRigidBody.AddForce ( gravityVector * ( orbitalDistance/proximityModifier) );


				} 
			}

		}
	}
}
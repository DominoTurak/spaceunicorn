﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager> {

	public GameObject Ship;
	public Transform StartingPoint;
	public float MapWidth;

	public Transform MapBoundaryLeft;
	public Transform MapBoundaryRight;

	public bool isPlaying;

	public GameObject gameOverGUI;

	public void Start(){
		StartCoroutine(OnSceneLoad());
	}

	public void Update(){

		if (isPlaying) {
			UpdateBoundaries ();
		}

	}
		

	private IEnumerator OnSceneLoad(){
		yield return new WaitForEndOfFrame ();
		StartGame ();
	}


	public void StartGame(){

		if (isPlaying) {
			PlanetsController.Instance.ResetAll ();
			FuelController.Instance.ResetAll ();
		}

		if (gameOverGUI.activeSelf) {
			gameOverGUI.SetActive (false);
		}



		Ship.transform.position = StartingPoint.position;
		Ship.transform.rotation = Quaternion.Euler (Vector3.zero);
		Ship.gameObject.SetActive (true);
		Ship.GetComponent<ShipController> ().OnStartGame ();


		isPlaying = true;
	}


	public void StopGame(){
		Ship.SetActive (false);
		//isPlaying = false;
	}


	private void UpdateBoundaries(){

		if (Ship.gameObject.activeSelf) {
			MapBoundaryLeft.transform.position = new Vector3 (-MapWidth / 2, Ship.transform.position.y, 0);
			MapBoundaryRight.transform.position = new Vector3 (MapWidth / 2, Ship.transform.position.y, 0);
		}



	}


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShipController : MonoBehaviour {

	private Rigidbody2D rigidbody;

	public GameObject chemtrails;
	public GameObject damageParticles;
	public GameObject explosion;

	public Vector2 thrust;
	public float rotationSpeed;

	public Vector2 maxVelocity;

	public float damageModifier = 1.4f;
	public float minFuel = 15;
	public float maxFuel = 30;


	private Vector2 startTouch;
	private bool isTouching;

	void Start () {
		this.rigidbody = this.GetComponent<Rigidbody2D> ();
	}


	// Update is called once per frame
	void FixedUpdate () {



		if (Input.GetMouseButton (0)) {

			if (fuel > 0) {
				fuel -= 10f * Time.fixedDeltaTime;

				fuel = fuel < 0 ? 0 : fuel;


				Vector3 direction = Vector3.Normalize(Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position);
				Vector2 resultDirection = new Vector2 (direction.x, direction.y);
				UpdateThrust (resultDirection, 1);
			}

		}

		UpdateRotation ();
		//VelocityReduction ();
		DebugFunctions();
		CheckShipHP();

	}

	private void UpdateThrust(Vector2 direction, float magnitude){
		Vector2 resultThrust = new Vector2(direction.x * thrust.x * magnitude, direction.y * thrust.y * magnitude);
		rigidbody.AddForce (resultThrust);

	}

	private void UpdateRotation(){

		float target_angle = Vector2.Angle (Vector2.up, rigidbody.velocity);
		if (rigidbody.velocity.x > 0) {
			target_angle = -target_angle;
		}

		rigidbody.MoveRotation( target_angle);
	}

	private void VelocityReduction(){
		Vector2 velocityReduction = new Vector2 ();

		if (Mathf.Abs(rigidbody.velocity.x) > maxVelocity.x || Mathf.Abs(rigidbody.velocity.y) > maxVelocity.y ){
			velocityReduction.x = maxVelocity.x / Mathf.Abs(rigidbody.velocity.x);
			velocityReduction.y = maxVelocity.y / Mathf.Abs(rigidbody.velocity.y);

			velocityReduction.x = velocityReduction.x > 1 ? velocityReduction.x : 1;
			velocityReduction.y = velocityReduction.y > 1 ? velocityReduction.y : 1;

			rigidbody.velocity = Vector2.Scale (rigidbody.velocity, velocityReduction);

		}

	}

	public void OnCollisionEnter2D(Collision2D coll){

		if (coll.gameObject.tag == "Planet") {
			float damage = Vector2.SqrMagnitude (coll.relativeVelocity);

			ContactPoint2D[] contacts = new ContactPoint2D [30];
			rigidbody.GetContacts (contacts);
			float biggestImpulse = 0;
			for (int i = 0; i < contacts.Length; i++) {
				float impulse = contacts [i].normalImpulse;
				if (impulse > biggestImpulse) {
					AnimateDamage(contacts[i].point);
					biggestImpulse = impulse;
				}
			}

			damage = biggestImpulse * damageModifier;

			if (true) {
				health -= damage;
				health = health < 0 ? 0 : health;
				if (health <= 0) {
					this.gameObject.SetActive (false);
					GUIManager.Instance.SetGameOver ();
				}
			}
		}
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Fuel") {
			fuel += Random.Range (minFuel, maxFuel);
			fuel = fuel > 100 ? 100 : fuel;

			FuelController.Instance.DeactiveFuel (other.gameObject);
		}
	}

	private void CheckShipHP()
	{
		if (health < 30)
			damageParticles.SetActive(true);
		else
			damageParticles.SetActive(false);
	}


	private void AnimateDamage(Vector3 position)
	{
		var explosionPrefab = Instantiate(explosion);
		explosionPrefab.transform.localPosition = position;

		StopCoroutine(DeactiveDamage(explosionPrefab));
		StartCoroutine(DeactiveDamage(explosionPrefab));
	}

	private IEnumerator DeactiveDamage(GameObject go)
	{
		yield return new WaitForSeconds(1);
		go.SetActive(false);
	}



	#region GUI Manager
	private float health = 100;
	private float fuel = 100;

	public void OnStartGame(){
		health = 100;
		fuel = 100;

		this.rigidbody.AddForce (Vector2.up * 10f, ForceMode2D.Impulse);
	}

	private void DebugFunctions()
	{
		if (Input.GetKeyDown(KeyCode.H))
		{
			health -= 10;
		}

		if (Input.GetKeyDown(KeyCode.F))
		{
			fuel -= 10;
		}

	}

	public int GetShipPosition()
	{
		return (int)transform.position.y;
	}

	public bool IsGameOver()
	{
		return health <= 0;
	}

	public float GetCurrentHealth()
	{
		return health;
	}

	public float GetCurrentFuel()
	{
		return fuel;
	}

	#endregion

}

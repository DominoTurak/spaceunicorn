﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour {

	Material mat;
	Vector3 lastCamPosition;

	public float scrollSpeed;

	// Use this for initialization
	void Start () {

		this.mat = this.GetComponent<MeshRenderer> ().material;
		lastCamPosition = Camera.main.transform.position;
		
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 currentCamPosition = Camera.main.transform.position;
		Vector2 CamPosDelta = new Vector2 ( mat.mainTextureOffset.x  - (currentCamPosition.x - lastCamPosition.x) * scrollSpeed,  mat.mainTextureOffset.y - (currentCamPosition.y - lastCamPosition.y) * scrollSpeed) ;

		mat.SetTextureOffset ("_MainTex", CamPosDelta);

		lastCamPosition = currentCamPosition;


		
	}
}

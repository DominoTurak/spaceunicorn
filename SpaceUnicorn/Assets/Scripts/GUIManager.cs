﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GUIManager : Singleton<GUIManager>
{

	public ShipController ship;

	private int startTime;

	#region GameScreen

	public Text currentDistance;
	public Text currentTime;
	#endregion

	#region GameOver
	public GameObject gameOver;

	public Text distance;
	public Text bonus;
	public Text total;

	#endregion

	public Image fuelBar;
	public Image healtBar;


	// Use this for initialization
	void Start()
	{
		startTime = -1;
	}

	// Update is called once per frame
	void Update()
	{
		if (ship.GetShipPosition() > 0)
		{
			if (startTime == -1)
			{
				startTime = (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds;
				ship.chemtrails.SetActive(true);
			}

			var currentTimeTmp = (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds - startTime;
			currentDistance.text = ship.GetShipPosition().ToString();
			currentTime.text = currentTimeTmp.ToString();
		}

		fuelBar.rectTransform.localScale = new Vector3( (ship.GetCurrentFuel() / 100.0f), 0.95f, 1);
		healtBar.rectTransform.localScale = new Vector3((ship.GetCurrentHealth() / 100.0f), 0.95f, 1);
	}

	public void SetGameOver(){
		distance.text = currentDistance.text;
		float timeForBonus = int.Parse (currentTime.text) == 0 ? 1 : int.Parse (currentTime.text);
		var bonusValue = (int)(int.Parse (currentDistance.text) / timeForBonus);
		bonus.text = bonusValue.ToString ();
		total.text = (int.Parse (currentDistance.text) + bonusValue).ToString ();
		StartCoroutine (StartGameOver ());

	}

	public IEnumerator StartGameOver(){
		yield return new WaitForSeconds(1);
		gameOver.SetActive(true);
		startTime = -1;


	}

	public void BackToMenu()
	{
		SceneManager.LoadScene(0);
	}


}
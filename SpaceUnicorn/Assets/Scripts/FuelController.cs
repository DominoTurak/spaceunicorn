﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelController : Singleton<FuelController> {

	private List<GameObject> ActiveFuels;
	private List<GameObject> InactiveFuels;

	public GameObject fuelPrefab;
	public int initialFuels;

	public void Start(){
		ActiveFuels = new List<GameObject> ();
		InactiveFuels = new List<GameObject> ();

		for (int i = 0; i < initialFuels; i++) {
			GameObject newFuel = GameObject.Instantiate (fuelPrefab, new Vector3(-200,0,0), Quaternion.Euler (Vector3.zero), this.transform);
			InactiveFuels.Add (newFuel);
		}
	}


	public void Update(){
		if (Input.GetMouseButtonDown (1)) {
			Vector3 position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			position.z = 0;
			ActivateFuel (position);

		}

	}

	public void ActivateFuel(Vector3 position){
		GameObject fuelToActivate = null;

		if (InactiveFuels.Count > 0) {
			fuelToActivate = InactiveFuels [0];
			InactiveFuels.Remove (fuelToActivate);
			ActiveFuels.Add (fuelToActivate);

		} else {
			fuelToActivate = ActiveFuels [0];
			ActiveFuels.Remove (fuelToActivate);
			ActiveFuels.Add (fuelToActivate);
		}

		fuelToActivate.SetActive (true);
		fuelToActivate.transform.position = position;


		Vector2 forceDirection = new Vector2 (Random.Range (1, -1), Random.Range (-1, 1));

		fuelToActivate.GetComponent<Rigidbody2D> ().AddForce (forceDirection * 0.6f, ForceMode2D.Impulse);

		float rotation = Random.Range (-1, 1) > 0 ? 1 : -1;

		fuelToActivate.GetComponent<Rigidbody2D> ().AddTorque (rotation, ForceMode2D.Impulse);

	}

	public void DeactiveFuel(GameObject fuel){

		ActiveFuels.Remove (fuel);
		fuel.SetActive (false);
		InactiveFuels.Add (fuel);
	}

	public void ResetAll(){
		for (int i = ActiveFuels.Count - 1; i >= 0; i--) {
			DeactiveFuel (ActiveFuels [i]);
		}
			

	}

}

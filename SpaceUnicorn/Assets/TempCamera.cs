﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempCamera : MonoBehaviour {

	private Transform SpaceShip;
	private Rigidbody2D spaceship_rigid;

	private float MapWidth;

	public float distance;
	public float cameraBoundsOffset;

	public int minCameraSize;
	public int maxCameraSize;

	public float minSpeed;
	public float maxSpeed;


	private Vector2 cameraXBounds;


	// Use this for initialization
	void Awake() {
		
		Init ();
	}

	private void Init(){
		this.SpaceShip = GameManager.Instance.Ship.transform;
		this.MapWidth = GameManager.Instance.MapWidth;
		this.spaceship_rigid = SpaceShip.GetComponent<Rigidbody2D> ();


		cameraXBounds = new Vector2 (-MapWidth + MapWidth * cameraXBounds.x, MapWidth - MapWidth * cameraXBounds.y);


	}
	
	// Update is called once per frame
	void Update () {

		if (SpaceShip == null) {
			Init ();
		}



		Vector3 newPosition = new Vector3 ();

		float shipPercentX = SpaceShip.position.x / MapWidth/2;

		float cameraXPos = (Mathf.Abs (cameraXBounds.x) + Mathf.Abs (cameraXBounds.y) / 2) * shipPercentX;


		newPosition = SpaceShip.transform.position;
		newPosition.x = cameraXPos;
		newPosition.z = this.transform.position.z;
		newPosition.y = newPosition.y + distance;

		if(cameraXPos != null){
			this.transform.position = newPosition;
		}


	
		
	}

	private void UpdateCamera(){










	}
}
